import argparse
import configparser
import os

from collections import OrderedDict
from pathlib import Path
from pprint import pprint


class Service():
    def __init__(self, name):
        self.actions = {
            "Unit": {
                "description": self._description,
                "documentation": self._documentation,
                "before": self._before,
                "after": self._after,
                "wants": self._wants,
                "requires": self._requires,
                "conditioncapability": self._conditioncapability,
                "conditionvirtualization": self._conditionvirtualization,
                "defaultdependencies": self._defaultdependencies,
                "conflicts": self._conflicts,
            },
            "Service": {
                "type": self._type,
                "execstart": self._execstart,
                "execstop": self._execstop,
                "execreload": self._execreload,
                "busname": self._busname,
                "killmode": self._killmode,
                "standarderror": self._standarderror,
                "restart": self._restart,
                "capabilityboundingset": self._capabilityboundingset,
                "protectsystem": self._protectsystem,
                "protecthome": self._protecthome,
                "privatetmp": self._privatetmp,
                "restrictaddressfamilies": self._restrictaddressfamilies,
                "nonewprivileges": self._nonewprivileges,
                "user": self._user,
                "ambientcapabilities": self._ambientcapabilities,
                "lockpersonality": self._lockpersonality,
                "memorydenywriteexecute": self._memorydenywriteexecute,
                "privatedevices": self._privatedevices,
                "protectcontrolgroups": self._protectcontrolgroups,
                "protecthostname": self._protecthostname,
                "protectkernelmodules": self._protectkernelmodules,
                "protectkerneltunables": self._protectkerneltunables,
                "protectkernellogs": self._protectkernellogs,
                "restartsec": self._restartsec,
            },
            "Install": {
                "alias": self._alias,
                "wantedby": self._wantedby,
            },
        }
        self.name = name

    def _description(self, value):
        self.description = value
    def _documentation(self, value):
        pass
    def _before(self, value):
        pass
    def _after(self, value):
        pass
    def _wants(self, value):
        pass
    def _requires(self, value):
        pass
    def _conditioncapability(self, value):
        pass
    def _conditionvirtualization(self, value):
        pass
    def _defaultdependencies(self, value):
        pass
    def _conflicts(self, value):
        pass

    def _execstart(self, value):
        pass
    def _execstop(self, value):
        pass
    def _execreload(self, value):
        pass
    def _busname(self, value):
        pass
    def _type(self, value):
        pass
    def _killmode(self, value):
        pass
    def _standarderror(self, value):
        pass
    def _restart(self, value):
        pass
    def _capabilityboundingset(self, value):
        pass
    def _protectsystem(self, value):
        pass
    def _protecthome(self, value):
        pass
    def _privatetmp(self, value):
        pass
    def _restrictaddressfamilies(self, value):
        pass
    def _nonewprivileges(self, value):
        pass
    def _user(self, value):
        pass
    def _ambientcapabilities(self, value):
        pass
    def _lockpersonality(self, value):
        pass
    def _memorydenywriteexecute(self, value):
        pass
    def _privatedevices(self, value):
        pass
    def _protectcontrolgroups(self, value):
        pass
    def _protecthostname(self, value):
        pass
    def _protectkernelmodules(self, value):
        pass
    def _protectkerneltunables(self, value):
        pass
    def _protectkernellogs(self, value):
        pass
    def _restartsec(self, value):
        pass

    def _alias(self, value):
        pass
    def _wantedby(self, value):
        pass

    def parse_config_line(self, section, option, value):
        action_func  = self.actions.get(section, dict()).get(option, None)
        if action_func is not None:
            action_func(value)
        else:
            raise ValueError(f"Unknown option [{section}]{option} in {self.name}")


# https://pastebin.com/cZ8SzbXK
class MultiDict(dict):
     def __setitem__(self, key, value):
        if isinstance(value, list) and key in self:
            self[key].extend(value)
        else:
            super().__setitem__(key, value)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Python init and services manager")
    parser.add_argument("-c", "--config", type=str, required=True,
        help="Config file path")

    args = parser.parse_args()
    config = configparser.ConfigParser()
    config.read(args.config)
    services_folder = Path(config["main"]["services_folder"])
    services = os.listdir(services_folder)
    pprint(services)

    for service_name in services:
        service = services_folder / Path(service_name)
        if service.is_file():
           service_parser = configparser.RawConfigParser(dict_type=MultiDict, strict=False)
           service_parser.read(service)
           service_obj = Service(service_name)
           for section in service_parser.sections():
               for option, value in service_parser[section].items():
                   value = value.split("\n")
                   service_obj.parse_config_line(section, option, value)
           del service_parser
